import { Injectable } from '@angular/core';
import { LoadingController, App } from 'ionic-angular';

/*
  Generated class for the Common provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Common {
  public loader: any;
  constructor(public loadingCtrl: LoadingController,public app: App) {
  }

  presentLoading(){
    this.loader = this.loadingCtrl.create({content: "Please wait ..."})
    this.loader.present();
  }

  closeLoading(){
  this.loader.dismiss();
  }

  logout() {
    //Api Token Logout  
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 1000);
  }

  backToWelcome() {
    const root = this.app.getRootNav();
    root.popToRoot();
  }

}
