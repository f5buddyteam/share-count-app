import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from './global/global';

let apiUrl = "http://localhost/sharecount-android/";
//let apiUrl = 'https://api.thewallscript.com/restful/';
//let apiUrl = 'http://mywebprovider.com/sharecount/';


@Injectable()
export class AuthService {
  constructor(public http: Http, public global: GlobalProvider) {

}

  postData(credentials, type){ 
    credentials.user_id = this.global.userIdGlobalVar;
    credentials.token = this.global.TokenGlobalVar;
    
    return new Promise((resolve, reject) =>{
      let headers = new Headers();
      this.http.post(apiUrl+type, credentials, {headers: headers}).
      subscribe(res =>{
        resolve(res.json());
      }, (err) =>{
        reject(err);
      });

    });

  }

}
