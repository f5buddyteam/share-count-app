import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
   ToastController, AlertController,
    ViewController, ActionSheetController } from 'ionic-angular';

import { FormBuilder } from '@angular/forms';

import { AuthService } from '../../providers/auth-service';
import { Common } from '../../providers/common';
import { Item } from '../../models/item';
import { Items } from '../../mocks/providers/items';
import { GlobalProvider } from '../../providers/global/global';

@IonicPage()
@Component({
  selector: 'page-view-single-event',
  templateUrl: 'view-single-event.html',
})
export class ViewSingleEventPage {
  create_by: any;
  end_date: any;
  start_date: any;
  eventname: any;
  event: any;
  total_expenses: any;
  eventUser: any;
  event_id: any;
  public userDetails: any;
  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;
  userPostData = {
    event_id: "",
    first_name: "",
    mobile: ""
  };
  nacPar = {
    event_id: "",
    ex_user_id: ""
  };
  public uesr_id:any;

  constructor(
    public authService: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public common: Common,
    public items: Items,
    public global: GlobalProvider,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.event_id = navParams.get("event_id");
    if (this.event_id){
      this.userPostData.event_id = this.event_id;
      this.getEvent();
    }else{     
        this.navCtrl.popToRoot();   
    }
    this.uesr_id = this.global.userIdGlobalVar;
  }

  ionViewDidLoad() {
   
  }


  getEvent() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_single_event").then(
      result => {
        this.resposeData = result;     
          this.common.closeLoading();
        console.log(this.resposeData)
          if (this.resposeData.data) {
            this.eventUser = this.resposeData.data.users;
            this.total_expenses = this.resposeData.data.total.total_expenses;
            this.event = this.resposeData.data.events;
            this.eventname = this.resposeData.data.events.name;
            this.start_date = this.resposeData.data.events.start_date;
            this.end_date = this.resposeData.data.events.end_date;
            this.create_by = this.resposeData.data.events.create_by;
          
          } else {
              this.navCtrl.popToRoot();
          }
        
      },
      err => {

        //Connection failed message
      }
    );
  } 


  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      //title: 'Modify ',
      buttons: [
        {
          text: 'Add New Person',
          handler: () => {
            this.addEventMember();
          }
        }, {
          text: 'Edit',
          handler: () => {
          
            this.navCtrl.push('EditEventPage', { 'event_id': this.event_id });
          }
        }, {
          text: 'Delete',
          handler: () => {
            this.deleteEvent();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  addEventMember() {
    let alert = this.alertCtrl.create({
      title: 'Person Details',
      inputs: [
        {
          name: 'first_name',
          placeholder: 'Name'
        },
        {
          name: 'mobile',
          placeholder: 'Mobile',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            if (data.first_name != "") {
              if (data.mobile.length == 10) {     
                this.common.presentLoading();
                  this.userPostData.first_name = data.first_name;
                  this.userPostData.mobile = data.mobile;
                  this.authService.postData(this.userPostData, "invitation").then(
                    result => {
                      this.common.closeLoading();
                      this.resposeData = result;
                      if (this.resposeData.status == 200) {
                        let myDataArray = { "first_name": data.first_name, "mobile": data.mobile };
                        this.eventUser.push(new Item(myDataArray));                   
                      
                      } else {

                      }
                  },
                  err => {
                    //Connection failed message
                  }
                );
              } else {
                let toast = this.toastCtrl.create({
                  message: 'Please enter a valid 10 digit mobile number',
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
                return false;
              }
            } else {
              let toast = this.toastCtrl.create({
                message: 'Please enter member name',
                duration: 3000,
                position: 'top'
              });
              toast.present();
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  Addexpense(member) {
    //return false;
    this.event.invite_id = member;
    this.event.eventUser = this.eventUser;
    //console.log(this.event)
    this.navCtrl.push('AddExpensesPage', { 'event': this.event });
  }

  deleteEvent() {
      let alert = this.alertCtrl.create({
        title: "Delete Event",
        message: "Do you want to delete this event?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              
            }
          },
          {
            text: "Delete",
            handler: () => {
              this.common.presentLoading();
              this.authService.postData(this.userPostData, "delete_event").then(
                result => {
                  this.common.closeLoading();
                  this.resposeData = result;
                  if (this.resposeData.status == 200) {
                    this.navCtrl.push('HomePage');
                  }
                },
                err => {
                  this.common.closeLoading();
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }



  expenseSingleUser(id) { 
    this.nacPar.event_id = this.event_id;
    this.nacPar.ex_user_id = id;
    this.navCtrl.push('IndividualExpensesListPage', { 'details': this.nacPar });
  }


  expenseSummary() {
    this.navCtrl.push('ExpenseSummaryPage', { 'details': this.resposeData.data });
  }

  expenseDetails() {
    this.navCtrl.push('ExpensedetailsPage', { 'details': this.resposeData.data });
  }

  expenseChart() { 
   alert(1)
    //this.navCtrl.push('test', { 'details': this.resposeData.data });
  }


  
}
