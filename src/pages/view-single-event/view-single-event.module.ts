import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewSingleEventPage } from './view-single-event';

@NgModule({
  declarations: [
    ViewSingleEventPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewSingleEventPage),
  ],
})
export class ViewSingleEventPageModule {}
