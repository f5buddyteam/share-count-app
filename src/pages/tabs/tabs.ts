import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  

  tab1Root = HomePage;
  tab2Root = AboutPage;
//  tab3Root = ContactPage;
  tab3Root = ProfilePage;

  constructor(public global: GlobalProvider,) {
    setInterval(this.login_check(), 300);
  }
  login_check(){
    if (this.global.TokenGlobalVar==''){
      
    }
  }


}
