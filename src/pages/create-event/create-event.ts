import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ViewController } from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Item } from '../../models/item';
import { Items } from '../../mocks/providers/items';
import { AuthService } from '../../providers/auth-service';
import { TabsPage } from '../tabs/tabs';
import { Common } from '../../providers/common';
 
@IonicPage()
@Component({
  selector: 'page-create-event',
  templateUrl: 'create-event.html',
})
export class CreateEventPage {
  token: any;
  data: any;
  resposeData:any;
  myData: any;
  isReadyToSave: boolean;
  form: FormGroup;
  eventUser:any;
  Sdatearray:any;
  Edatearray:any;
  
  constructor(
    public items: Items,
    public authService: AuthService,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public common: Common,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder
    
  ) {

    this.resposeData = {
      status: "",
      message: ""
    };

    this.data = {
      token: ""
    };

    /* const data = JSON.parse(localStorage.getItem("userData"));
    this.token = this.data.token; */
      
    this.eventUser = this.items.query();
    if(this.eventUser.length<1){
      for (let item of this.eventUser) {
        this.items.delete(item);
      }
      this.eventUser = this.items.query();
    }

   // console.log(this.eventUser)
  
    this.myData = null;
    this.form = formBuilder.group({
      //profilePic: [''],
      name: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      description: ['',Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {
    this.eventUser = this.items.query();
    for (let item of this.eventUser) {
      this.items.delete(item);
    }
  }

  addEventMember() {
    let alert = this.alertCtrl.create({
      title: 'Person Details',
      inputs: [
        {
          name: 'first_name',
          placeholder: 'Name'
        },
        {
          name: 'mobile',
          placeholder: 'Mobile',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            
          }
        },
        {
          text: 'Add',
          handler: data => {
            if (data.first_name != "") {
              if (data.mobile.length == 10) {
                let myDataArray = { "first_name": data.first_name, "mobile": data.mobile };
                this.items.add(new Item(myDataArray));
               
              } else {
                this.presentToast('Please enter a valid 10 digit mobile number');
                return false;
              }
            } else {
              this.presentToast('Please enter member name');             
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

 deleteEventmember(item) {
    this.items.delete(item);
  }


  createEvent(form) {
  
    let dateOne = new Date(this.form.controls.start_date.value); //Year, Month, Date  
    let dateTwo = new Date(this.form.controls.end_date.value); //Year, Month, Date  
    if (dateOne > dateTwo) {  
      this.presentToast('Please select end date greater than start date');
    }else { 
    if (this.form.value.name !== "") {
      if (this.eventUser.length > 0) {
        this.form.value.eventUser = this.eventUser;
        this.authService.postData(this.form.value, "add_event").then((result) => {
          this.resposeData = result;
          if (this.resposeData.status == 200) {
            this.presentToast('Event has been created successfully');           
            this.navCtrl.push(TabsPage);
          } else if (this.resposeData.status == 201) {
            this.common.logout();
          }
        }, (err) => {
          //Connection failed message
        });
      }else{        
        this.presentToast("Please select member for this event");
      }
    }else{

    } 
  } 
  }  

  

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
