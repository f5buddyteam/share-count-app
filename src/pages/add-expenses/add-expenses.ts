import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,ViewController,ActionSheetController } from 'ionic-angular';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth-service';
import { Common } from '../../providers/common';

@IonicPage()
@Component({
  selector: 'page-add-expenses',
  templateUrl: 'add-expenses.html',
})
export class AddExpensesPage {
 
  @ViewChild('fileInput') fileInput;
  isReadyToSave: boolean;
  form: FormGroup;
  event: any;
  myData: any;
  invite_id: any;
  event_id: any;
  eventname: string;
  start_date: any;
  end_date: any;
  create_by: any;
  resposeData: any;
  eventUser: any;
  alluser:any;

  constructor(
    public authService: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public common: Common,
    public actionSheetCtrl: ActionSheetController
  ) {
    this.event = navParams.get("event");
    if(!this.event){
      this.navCtrl.popToRoot();
    }

    this.invite_id = this.event.invite_id;
    this.event_id = this.event.id;

    this.eventname = this.event.name;
    this.start_date = this.event.start_date;
    this.end_date = this.event.end_date;
    this.create_by = this.event.create_by;
    this.eventUser = this.event.eventUser;

    this.myData = null;
    this.form = formBuilder.group({
      title: ['', Validators.required],
      date: ['', Validators.required],
      money: ['', Validators.required],      
      distributeUser: ['' ],
      description: ['', Validators.required]
    });
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;

    });

    this.alluser = false;
}

  toggleOnOff(e){
    if (this.alluser) {
      this.alluser = false;
    }else{
      this.alluser = true;
    }    
  }


  addexpense(form) {
    this.form.value.invite_id = this.invite_id;
    this.form.value.event_id = this.event_id;
    if (!this.alluser){     
      let array1 = ['All']; 
      this.form.value.distributeUser = array1;
    }

    if (this.alluser && this.form.value.distributeUser == "" || this.form.value.distributeUser ==undefined){
        this.presentToast('Please select share with users');
        return false;
    } 
  
    
    this.common.presentLoading();
    this.authService.postData(this.form.value, "add_expenses").then(
      result => {
        this.common.closeLoading();
        this.resposeData = result;
      
        if (this.resposeData.status == 200 ) { 
          //this.navCtrl.popToRoot();
          //console.log(this.resposeData.data);
          this.presentToast(this.resposeData.data);
          this.navCtrl.push('ViewSingleEventPage', { event_id: this.event_id });
        } 
        
      },
      err => {
        this.common.closeLoading();
      }
    );

    } 

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  }
