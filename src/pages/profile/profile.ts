import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, App, ToastController } from 'ionic-angular';

import { AuthService } from '../../providers/auth-service';
import { Common } from '../../providers/common';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  base64Image: string;
  userPostData = {
    user_id: "",
    token: ""
  };

  userData = { profilepic: "", last_name: "", email: "", first_name: "" };
  public resposeData: any;


  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;
  isEdit: boolean;
  item: any;
  form: FormGroup;
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public camera: Camera,
    public common: Common,
    public authService: AuthService,
    public toastCtrl: ToastController,
    public app: App
  ) {
    this.form = formBuilder.group({
      profilepic: [''],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
    this.getProfile();
  }

  getProfile() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_profile").then(
      result => {
        this.common.closeLoading();

        this.resposeData = result;
        console.log(this.resposeData)
        if (this.resposeData.status == 200) {
          if (this.resposeData.data) {
            console.log(this.resposeData.data)
            this.base64Image = this.resposeData.data.profilepic;
            this.userData.first_name = this.resposeData.data.first_name;
            this.userData.last_name = this.resposeData.data.last_name;
            this.userData.email = this.resposeData.data.email;
            this.processWebImage1(this.base64Image)

          }
        } else if (this.resposeData.status == 201) {
          this.common.logout();
        }
      },
      err => {

      }
    );
  }

  processWebImage1(event) {
    this.form.patchValue({ 'profilepic': event });
  }


  editButton() {
    this.isEdit = true;
  }

  editButton1() {
    this.isEdit = false;
  }


  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({ 'profilepic': 'data:image/jpg;base64,' + data });
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }


  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {
      let imageData = (readerEvent.target as any).result;

      this.form.patchValue({ 'profilepic': imageData });
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilepic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    //if (!this.form.valid) { return; }   
    if (!this.form.valid) {
      this.presentToast('First Name and Last Name is required');
      return;
    }
    this.common.presentLoading();
    this.authService.postData(this.form.value, "update_profile").then(
      result => {
        this.common.closeLoading();
        this.resposeData = result;
        if (this.resposeData.status == 200) {
          this.presentToast('Profile has been updated successfully');
        }
      },
      err => {
        //Connection failed message
      }
    );
    //this.viewCtrl.dismiss(this.form.value);
  }

  logout() {
    this.common.logout();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}