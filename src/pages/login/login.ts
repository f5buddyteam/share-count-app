import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import {AuthService} from "../../providers/auth-service";
import { GlobalProvider } from '../../providers/global/global';
import { Common } from '../../providers/common';
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  otpInput:boolean;
  resposeData : any;
  userData = {'mobile': '','otp': '' };

  constructor(public navCtrl: NavController,
    public authService: AuthService,
    public common: Common,
    public global: GlobalProvider,
    private toastCtrl:ToastController) {
    this.otpInput = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  login(){
   if(this.userData.mobile.length == 10 ){
     this.common.presentLoading();
    this.authService.postData(this.userData, "login").then((result) =>{
    this.resposeData = result;
      this.common.closeLoading();
      if (this.resposeData.status==200){
      this.otpInput = true;
      this.presentToast(this.resposeData.Description);
      // localStorage.setItem('userData', JSON.stringify(this.resposeData) )
      //this.navCtrl.push(TabsPage);
      } else if (this.resposeData.status == 500){
        this.presentToast(this.resposeData.errorDescription);
      }
    else{
      this.presentToast("Please enter valid mobile number");
    } 
    }, (err) => {
      //Connection failed message
    });
   }
   else{
    this.presentToast("Enter your Mobile");
   }
  
  }

  verifyOTP() {
    if (this.userData.otp.length==6) {
      this.common.presentLoading();
      this.authService.postData(this.userData, "verify_otp").then((result) => {
        this.resposeData = result;
        this.common.closeLoading();
        if (this.resposeData.status == 200) {         
          this.presentToast(this.resposeData.Description);
          this.global.TokenGlobalVar = this.resposeData.token;
          this.global.userIdGlobalVar = this.resposeData.user_id;
          localStorage.setItem('userData', JSON.stringify(this.resposeData))                
          this.navCtrl.push(TabsPage); 
        } else if (this.resposeData.status == 500) {
          this.presentToast(this.resposeData.errorDescription);
        }
        else {
          this.presentToast("Please enter valid OTP number");
        }
      }, (err) => {
        //Connection failed message
      });
    }
    else {
      this.presentToast("Please enter valid OTP number");
    }
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
