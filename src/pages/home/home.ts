import { Component, ViewChild } from "@angular/core";
import { NavController, App, AlertController, ToastController } from "ionic-angular";
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";


@Component({ selector: "page-home", templateUrl: "home.html" })
export class HomePage {
  token: any;
  @ViewChild("updatebox") updatebox;
 
  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;
  userPostData = {
    feed: "",
    feed_id: "",
    event_id: "",
    lastCreated: ""
  };

  constructor(
    public common: Common,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public app: App,
    public authService: AuthService
  ) {
    this.getEvent();
  }

  getEvent() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_event").then(
      result => {
        this.common.closeLoading();     
        this.resposeData = result;
        if (this.resposeData.status==200){
            if (this.resposeData.data) {
            this.dataSet = this.resposeData.data;
            const dataLength = this.resposeData.data.length;
              this.userPostData.lastCreated = this.resposeData.data[dataLength - 1].created;                
            } 
          }else if (this.resposeData.status == 201) {
            this.common.logout();
          }
      },
      err => {
        this.common.closeLoading();
        //Connection failed message
      }
    );
  } 

  doRefresh(refresher) {
    //console.log('Begin async operation', refresher);
    this.getEvent();
    setTimeout(() => {
      refresher.complete();
    }, 0);
  }


  createEvent() {
    this.navCtrl.push('CreateEventPage');
  }

  eventView(event_id) {
    this.navCtrl.push('ViewSingleEventPage', {
      event_id: event_id });
  }



  feedUpdate() {
    if (this.userPostData.feed) {
      this.common.presentLoading();
      this.authService.postData(this.userPostData, "feedUpdate").then(
        result => {
          this.resposeData = result;
          if (this.resposeData.feedData) {
            this.common.closeLoading();
            this.dataSet.unshift(this.resposeData.feedData);
            this.userPostData.feed = "";

            //this.updatebox.setFocus();
            setTimeout(() => {
              //  this.updatebox.focus();
            }, 150);
          } else {
            console.log("No access");
          }
        },
        err => {
          //Connection failed message
        }
      );
    }
  }

  eventDelete(event_id, msgIndex) {
    if (event_id > 0) {
      let alert = this.alertCtrl.create({
        title: "Delete Event",
        message: "Do you want to delete this event?",
        buttons: [
          {
            text: "Cancel",
            role: "cancel",
            handler: () => {
              
            }
          },
          {
            text: "Delete",
            handler: () => { 
              this.userPostData.event_id = event_id;
              this.authService.postData(this.userPostData, "delete_event").then(
                result => {
                  this.resposeData = result;
                  if (this.resposeData.status==200) {
                    this.presentToast(this.resposeData.data);
                    this.dataSet.splice(msgIndex, 1);
                  } 
                },
                err => {
                  //Connection failed message
                }
              );
            }
          }
        ]
      });
      alert.present();
    }
  }



  doInfinite(e): Promise<any> {
    console.log("Begin async operation");
    return new Promise(resolve => {
      setTimeout(() => {
        this.authService.postData(this.userPostData, "get_event").then(
          result => {
            this.resposeData = result;
            console.log(this.resposeData)
            if (this.resposeData.feedData.length) {
              const newData = this.resposeData.feedData;
              this.userPostData.lastCreated = this.resposeData.feedData[
                newData.length - 1
              ].created;

              for (let i = 0; i < newData.length; i++) {
                this.dataSet.push(newData[i]);
              }
            } else {
              this.noRecords = true;
              console.log("No user updates");
            }
          },
          err => {
            //Connection failed message
          }
        );
        resolve();
      }, 500);
    });
  }

  converTime(time) {
    let a = new Date(time * 1000);
    return a;
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}
