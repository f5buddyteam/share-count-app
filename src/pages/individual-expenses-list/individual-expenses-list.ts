import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Common } from '../../providers/common';
import { AuthService } from '../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-individual-expenses-list',
  templateUrl: 'individual-expenses-list.html',
})

export class IndividualExpensesListPage {
  first_name: any;
  userEx: any;
  details: any;
  total_expenses: any;
  userPostData = {
    event_id: "",
    ex_user_id:""
  };

  public resposeData: any;
  constructor(public navCtrl: NavController,
    public common: Common,
    public authService: AuthService,
    public navParams: NavParams) {

    this.details = navParams.get("details");   
   
    if (this.details.event_id && this.details.ex_user_id ) {
      this.userPostData.ex_user_id = this.details.ex_user_id;
      this.userPostData.event_id = this.details.event_id;

     this.getEventDetails();
    
    } else {
      this.navCtrl.popToRoot();
    }
  }


  getEventDetails() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_user_expenses").then(
      result => {
        this.resposeData = result;
        console.log(this.resposeData )
        this.common.closeLoading();
        if (this.resposeData.data) {

          this.userEx = this.resposeData.data.users;
          this.total_expenses = this.resposeData.data.total;
          if (this.userEx.length>0){  
            this.first_name = this.userEx[0].first_name;
          }

         // console.log(this.resposeData.data)
          /*  this.total_expenses = this.resposeData.data.total.total_expenses;
           this.event = this.resposeData.data.events;
           this.eventname = this.resposeData.data.events.name;
           this.start_date = this.resposeData.data.events.start_date;
           this.end_date = this.resposeData.data.events.end_date;
           this.create_by = this.resposeData.data.events.create_by; */

        } else {
          //this.navCtrl.pop();
        }

      },
      err => {

        //Connection failed message
      }
    );
  }

}
