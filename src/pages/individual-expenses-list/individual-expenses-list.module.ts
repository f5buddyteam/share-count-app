import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndividualExpensesListPage } from './individual-expenses-list';

@NgModule({
  declarations: [
    IndividualExpensesListPage,
  ],
  imports: [
    IonicPageModule.forChild(IndividualExpensesListPage),
  ],
})
export class IndividualExpensesListPageModule {}
