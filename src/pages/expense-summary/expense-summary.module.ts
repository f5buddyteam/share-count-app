import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpenseSummaryPage } from './expense-summary';

@NgModule({
  declarations: [
    ExpenseSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpenseSummaryPage),
  ],
})
export class ExpenseSummaryPageModule {}
