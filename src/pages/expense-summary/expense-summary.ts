import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Common } from '../../providers/common';
import { AuthService } from '../../providers/auth-service';


@IonicPage()
@Component({
  selector: 'page-expense-summary',
  templateUrl: 'expense-summary.html',
})
export class ExpenseSummaryPage {
  nacPar = {
    event_id:"",
    ex_user_id:""
  };
  details: any;
  create_by: any;
  end_date: any;
  start_date: any;
  eventname: any;
  event: any;
  eventUser: any;
  total_expenses: any;
  userPostData: any;
  public resposeData: any;

  constructor(public navCtrl: NavController, 
    public common:Common,
    public authService: AuthService,
    public navParams: NavParams) {

    this.details = navParams.get("details");
    console.log(this.details)
    if (this.details) {
      this.resposeData = this.details;
      this.eventUser = this.resposeData.users;
      this.total_expenses = this.resposeData.total.total_expenses;
      this.event = this.resposeData.events;
      this.eventname = this.resposeData.events.name;
      this.start_date = this.resposeData.events.start_date;
      this.end_date = this.resposeData.events.end_date;
      this.create_by = this.resposeData.events.create_by;
      
    } else {
      this.navCtrl.popToRoot();
    }
   

  }

   expenseSingleUser1(id) { 
     this.nacPar.event_id = this.event.id;
     this.nacPar.ex_user_id = id; 
     this.navCtrl.push('IndividualExpensesListPage', { 'details': this.nacPar });
  }
}