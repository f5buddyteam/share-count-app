import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Common } from '../../providers/common';
import { AuthService } from '../../providers/auth-service';


@IonicPage()
@Component({
  selector: 'page-expensedetails',
  templateUrl: 'expensedetails.html',
})
export class ExpensedetailsPage {
  details: any;
  eventname: any;
  eventEx: any;
  total_expenses: any;
  userPostData = {
    event_id: ""
  };
  event_id: any;
  public resposeData: any;

  constructor(public navCtrl: NavController,
    public common: Common,
    public authService: AuthService,
    public navParams: NavParams) {

    this.details = navParams.get("details");
   
   
    if (this.details.events.id) {  
        this.total_expenses = this.details.total.total_expenses;
        this.userPostData.event_id = this.details.events.id;
        this.getEventDetails();
    } else {
        this.navCtrl.popToRoot();
    }
  }

  getEventDetails() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_expenses").then(
      result => {
        this.resposeData = result;
        this.common.closeLoading();
        console.log(this.resposeData)
        if (this.resposeData.data) {
          this.eventEx = this.resposeData.data;
          //this.eventUser = this.resposeData.data.users;

         /*  this.total_expenses = this.resposeData.data.total.total_expenses;
          this.event = this.resposeData.data.events;
          this.eventname = this.resposeData.data.events.name;
          this.start_date = this.resposeData.data.events.start_date;
          this.end_date = this.resposeData.data.events.end_date;
          this.create_by = this.resposeData.data.events.create_by; */

        } else {
          //this.navCtrl.pop();
        }

      },
      err => {

        //Connection failed message
      }
    );
  }
}
