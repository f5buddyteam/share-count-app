import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpensedetailsPage } from './expensedetails';

@NgModule({
  declarations: [
    ExpensedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpensedetailsPage),
  ],
})
export class ExpensedetailsPageModule {}
