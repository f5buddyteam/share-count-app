import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, ViewController } from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Item } from '../../models/item';
import { Items } from '../../mocks/providers/items';
import { AuthService } from '../../providers/auth-service';
import { TabsPage } from '../tabs/tabs';
import { Common } from '../../providers/common';



@IonicPage()
@Component({
  selector: 'page-edit-event',
  templateUrl: 'edit-event.html',
})
export class EditEventPage {
  create_by: any;
  description: any;
  end_date: any;
  start_date: any;
  eventname: any;
  event_id: any;
  resposeData: any;
  myData: any;
  isReadyToSave: boolean;
  form: FormGroup;
  eventUser: any;
  userPostData = {
    event_id: "",
    invite_id:"",
  };
  constructor(
    public items: Items,
    public authService: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public common: Common,
    public formBuilder: FormBuilder) {

    this.event_id = navParams.get("event_id");
    if (this.event_id) {
      this.userPostData.event_id = this.event_id;
      this.getEvent()
    } else {
      this.navCtrl.popToRoot();
    }

    this.myData = null;
    this.form = formBuilder.group({
      //profilePic: [''],
      name: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
      description: ['',Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

  }

  getEvent() {
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "get_single_event").then(
      result => {
        this.resposeData = result;
        this.common.closeLoading();

        if (this.resposeData.data) {
            this.eventUser = this.resposeData.data.users;
            this.eventname = this.resposeData.data.events.name;
            this.start_date = this.resposeData.data.events.start_date;
            this.end_date = this.resposeData.data.events.end_date;
            this.create_by = this.resposeData.data.events.create_by;
            this.description = this.resposeData.data.events.description;

        } else if (this.resposeData.status == 201) {
        
        } else {
          this.navCtrl.popToRoot();
        }

      },
      err => {

        //Connection failed message
      }
    );
  } 



  deleteEventmember(member) {
    this.userPostData.invite_id = member;
    this.common.presentLoading();
    this.authService.postData(this.userPostData, "delete_event_user").then(
      result => {
        this.common.closeLoading();
        this.resposeData = result;
        if (this.resposeData.status == 200) {
          this.presentToast('Event has been removed successfully');
        } else if (this.resposeData.status == 201) {
          this.common.logout();
        }
      },
      err => {
        //Connection failed message
      }
    );
  }  


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

 

  createItem1(form) {
    let dateOne = new Date(this.form.controls.start_date.value); //Year, Month, Date  
    let dateTwo = new Date(this.form.controls.end_date.value); //Year, Month, Date  
    if (dateOne > dateTwo) {  
      this.presentToast('Please select end date greater than start date');
    }else {   
    if (this.form.value.name !== "") {
      if (this.eventUser.length > 0) {
        this.form.value.event_id = this.event_id;
        this.form.value.eventUser = this.eventUser;
        this.authService.postData(this.form.value, "update_event").then((result) => {
          this.resposeData = result;
          this.common.closeLoading();
          if (this.resposeData.status == 200) {
            this.presentToast('Event has been update successfully');
            this.navCtrl.push('ViewSingleEventPage', { event_id: this.event_id });
          } else if (this.resposeData.status == 201) {
            this.common.logout();
          }
        }, (err) => {
          this.common.closeLoading();
          //Connection failed message
        });
      } else {
        this.presentToast("Please select member for this event");
      }
    }
  }
  } 
}
