import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Login } from '../login/login';
import { Signup } from '../signup/signup';
import { TabsPage } from '../tabs/tabs';
import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the Welcome page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

let now = new Date(),
  minDate = new Date(now.getFullYear() - 100, 0, 1),
  maxDate = new Date(now.getFullYear() + 30, 11, 31),
  bday = new Date(now.getFullYear() - 18, now.getMonth(), now.getDay());


@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})

export class Welcome {
  year: number; 
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider) { 
    this.year = now.getFullYear();
  }

  login(){
   this.navCtrl.push(Login);
  }

  signup(){
   this.navCtrl.push(Signup, {}, {animate:false});
  }

}
