import {Component} from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {AuthService} from "../../providers/auth-service";

import {TabsPage} from '../tabs/tabs';
import {Login} from "../login/login";
import { GlobalProvider } from '../../providers/global/global';
import { Common } from '../../providers/common';

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({selector: 'page-signup', templateUrl: 'signup.html'})
export class Signup {
  resposeData : any;
  userData = { "mobile": "", "last_name": "", "email": "", "first_name": "", 'otp': ''};
 
  otpInput: boolean;
  constructor(public navCtrl : NavController,
    public common: Common,
     public authService : AuthService,
    public global: GlobalProvider,
    public toastCtrl:ToastController) {
    this.otpInput = false;
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  doSignup() {
    if (this.userData.mobile && this.userData.first_name && this.userData.last_name){
      //Api connections
      this.common.presentLoading();
      this.authService.postData(this.userData, "register").then((result) =>{
      this.resposeData = result;
      this.common.closeLoading();
      if (this.resposeData.status == 200) {
        this.otpInput = true;
          this.presentToast(this.resposeData.Description);  
        //localStorage.setItem('userData', JSON.stringify(this.resposeData) )
    
      } else if (this.resposeData.status == 202){
        this.presentToast(this.resposeData.errorDescription.invalid_mobile);  
      }else{
        this.presentToast("Invalid Mobile No.");
      }
    
    }, (err) => {
      //Connection failed message
    });
  }
  else {
      this.presentToast("Please fulfill all required field.");
  }
  
  }
  
  verifyOTP() {
    if (this.userData.otp.length == 6) {
      this.common.presentLoading();
      this.authService.postData(this.userData, "verify_otp").then((result) => {
        this.resposeData = result;
        this.common.closeLoading();
        if (this.resposeData.status == 200) {
          this.presentToast(this.resposeData.Description);
          this.global.TokenGlobalVar = this.resposeData.token;
          this.global.userIdGlobalVar = this.resposeData.user_id;
            localStorage.setItem('userData', JSON.stringify(this.resposeData) )
          this.navCtrl.push(TabsPage);
        } else if (this.resposeData.status == 500) {
          this.presentToast(this.resposeData.errorDescription);
        }
        else {
          this.presentToast("Please enter valid OTP number");
        }
      }, (err) => {
        //Connection failed message
      });
    }
    else {
      this.presentToast("Please enter valid OTP number");
    }
  }

  login() {
    this
      .navCtrl
      .push(Login);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
