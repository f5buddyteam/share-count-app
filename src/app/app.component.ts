import { Component } from '@angular/core';
import { Platform, App, MenuController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SplitPane } from '../providers/split-pane';
import { Welcome } from '../pages/welcome/welcome';
import { GlobalProvider } from '../providers/global/global';
import { TabsPage } from '../pages/tabs/tabs';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any = Welcome;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public app: App, public splitPane: SplitPane, public global: GlobalProvider, public toastCtrl: ToastController,
    public menu: MenuController) {

    if (localStorage.getItem("userData")) {
        const data = JSON.parse(localStorage.getItem("userData"));
      this.global.TokenGlobalVar = data.token;
      this.global.userIdGlobalVar = data.user_id;
      this.rootPage = TabsPage;
    } else {
      this.rootPage = Welcome;
    }

    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();      
    });

  /*   this.network.onDisconnect().subscribe(data => {
      

    }, error => console.error(error)); */
  }
}
