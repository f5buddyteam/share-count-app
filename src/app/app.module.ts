import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AuthService } from '../providers/auth-service';
import { Welcome } from '../pages/welcome/welcome';
import { Login } from '../pages/login/login';
import { Signup } from '../pages/signup/signup';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SplitPane } from '../providers/split-pane';
import { Common } from '../providers/common';

import { Api } from '../providers/api/api';
import { ProfilePage } from '../pages/profile/profile';
import { Camera } from '@ionic-native/camera';
import { Items } from '../mocks/providers/items';
import { HttpClient } from '@angular/common/http';




import { HttpClientModule } from '@angular/common/http';
import { GlobalProvider } from '../providers/global/global';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    ProfilePage,
    HomePage,
    Welcome,
    Login,
    Signup,
    TabsPage
    
  ],
  imports: [
    HttpClientModule,
    BrowserModule, HttpModule,  
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    Welcome,
    Login,
    Signup,
    ProfilePage,
    TabsPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen, AuthService, SplitPane, Common,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Items,
    Api,
    Camera,
    HttpClient,
    GlobalProvider
  ]
})
export class AppModule { }